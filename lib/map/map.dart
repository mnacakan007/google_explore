// import 'package:map_view/map_view.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:map_view/polyline.dart';
// import 'package:location/location.dart' as LocationManager;
// import 'package:cached_network_image/cached_network_image.dart';
import 'dart:async';
import 'package:access_settings_menu/access_settings_menu.dart';
import 'package:android_intent/android_intent.dart';
import 'package:showcaseview/showcase.dart';
import 'package:showcaseview/showcase_widget.dart';

import '../cards/data.dart';
// import 'map_coffee.dart';
import 'place_detail.dart';
import 'package:share/share.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';

// import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_search/cards/data.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_google_places/flutter_google_places.dart';


const kGoogleApiKey = "AIzaSyAPDJlw1bTeTllq_k1x6n5UZwnADszgTh0";
GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
dynamic bright = Brightness.light;
bool darkModeOn = false;

class Maps extends StatefulWidget {
  static const String routeName = "/map";

  @override
  State<StatefulWidget> createState() {
    return MapsState();
  }
}

class MapsState extends State<Maps> {

//  Map<String, double> currentLocation = new Map();
//  StreamSubscription<Map<String, double>> locationSubscription;
//  final location = LocationManager.Location();
//  String error;

  GlobalKey _one = GlobalKey();
  GlobalKey _two = GlobalKey();
  GlobalKey _three = GlobalKey();
  GlobalKey _four = GlobalKey();
  GlobalKey _five = GlobalKey();
  GlobalKey _six = GlobalKey();
  GlobalKey _seven = GlobalKey();
  GlobalKey _eight = GlobalKey();


  final homeScaffoldKey = GlobalKey<ScaffoldState>();
  PanelController _pc = new PanelController();
  List<PlacesSearchResult> placesData = [];
  MapType _currentMapType = MapType.normal;
  List<PlacesSearchResult> places = [];
  Geolocator geolocator = Geolocator();
  // final double _initFabHeight = 120.0;
  GoogleMapController mapController;
  dynamic buttonPositionTop = 150.0;
  // double _panelHeightOpen = 530.0;
  bool resultSettingsOpening = false;
  double _panelHeightClosed = 55.0;
  String name = "Search Location";
  String name2 = "Enter Address";
  PageController pageController;
  PlacesSearchResponse place2;
  PlacesDetailsResponse place;
  List<dynamic> popularResult;
  List<Marker> allMarkers = [];
  bool visibilityTag = false;
  String placeRadius = "500";
  bool currentWidget = true;
  Position userLocationNow;
  bool markersList = false;
  bool isLoading = false;
  Position userLocation;
  dynamic whereFrom = [];
  dynamic whereTo = [];
  // double _fabHeight;
  String errorMessage;
  dynamic placeDetail;
  GoogleMap googleMap;
  String popularPlace;
  String phoneNumber;
  dynamic myLocation;
  String searchAddr;
  dynamic imageList;
  List<dynamic> ccc;
  String titleText;
  double lat = 0.0;
  dynamic distance;
  double lng = 0.0;
  String website;
  int cardIndex;
  String image;
  int prevPage;
  dynamic res;
  dynamic pId;
  String url;
  dynamic id;
  var timer;



  @override
  void initState() {
    super.initState();
    _checkGps();
    _getLocation().then((position) {
      userLocation = position;
      setState(() {
        lat = userLocation.latitude;
        lng = userLocation.longitude;
        whereFrom = [lat, lng];
        isLoading = true;
        errorMessage = null;
      });
    });

//      getPlaceId();
//      getMyPosition();

//      fetchPlaceDetail(pId);

    geolocator
        .getPositionStream(LocationOptions(
        accuracy: LocationAccuracy.best, timeInterval: 1000))
        .listen((position) {
      userLocation = position;
    });

    pageController = PageController(initialPage: 1, viewportFraction: 0.8)
      ..addListener(_onScroll);

//    _fabHeight = _initFabHeight;

//    currentLocation['latitude'] = 0;
//    currentLocation['longitude'] = 0;
//    initPlatformState();
//    locationSubscription =
//        location.onLocationChanged().listen((Map<String, double> result) {
//          setState(() {
//            currentLocation = result;
//            lat = currentLocation['latitude'];
//            lng = currentLocation['longitude'];
//            print("currentLocation: $currentLocation");
//          });
//        });
//
//    print("currentLocation: $currentLocation");

  }

  void _onScroll() {
    if (pageController.page.toInt() != prevPage) {
      prevPage = pageController.page.toInt();
      moveCamera();
    }
  }

  _coffeeShopList(index) {

    return AnimatedBuilder(
      animation: pageController,
      builder: (BuildContext context, Widget widget) {
        double value = 1;
        if (pageController.position.haveDimensions) {
          value = pageController.page - index;
          value = (1 - (value.abs() * 0.3) + 0.06).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 125.0,
            width: Curves.easeInOut.transform(value) * 350.0,
            child: widget,
          ),
        );
      },
      child: InkWell(
          onTap: () {
            moveCamera();
          },
          child: Stack(children: [
            Center(
                child: Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: 10.0,
                      vertical: 20.0,
                    ),
                    height: 125.0,
                    width: 275.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black54,
                            offset: Offset(0.0, 4.0),
                            blurRadius: 10.0,
                          ),
                        ]),
                    child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white),
                        child: Row(children: [
                         placesData != [] ? Container(
                                height: 90.0,
                                width: 90.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(10.0),
                                        topLeft: Radius.circular(10.0)),
                                    image: DecorationImage(
                                        image: NetworkImage(
                                            buildPhotoURL(placesData[index]?.photos == null ? 'CmRaAAAASpwaTDoRcKz3t89e7gZ53jQMVG1LKb-OI7EwZZwbINt5IEiShHFFbFGraquLtDeqe_as0yKbZBLrx2kIIbXwWiN5vUNvhARMcyIYOIBm0rnE60SCKea89MZTITMjDYiVEhBHpnC__xoAuTVkOnr1pe53GhQpO2DeCTqq39-QZQJ_LFY_4Lr3ZQ' : placesData[index].photos[0].photoReference)),
                                        fit: BoxFit.cover))) : Container(),
                          SizedBox(width: 5.0),
                          Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 170.0,
                                  child: Text(
                                    placesData[index].name ?? '',
                                    style: TextStyle(
                                        fontSize: 12.5,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Container(
                                  width: 170.0,
                                  child: Text(
                                    placesData[index].types.first ?? '',
                                    style: TextStyle(
                                        fontSize: 11.0,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ),
                                Container(
                                  width: 170.0,
                                  child: Text(
                                    placesData[index].formattedAddress ?? '',
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                                Container(
                                  width: 170.0,
                                  child: Text(
                                    placesData[index].vicinity ?? '',
                                    style: TextStyle(
                                        fontSize: 11.0,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ),
                              ])
                        ]))))
          ])),
    );
  }

  @override
  Widget build(BuildContext context) {
    SharedPreferences preferences;

//    displayShowCase() async {
//      preferences = await SharedPreferences.getInstance();
//      bool showcaseVisibilityStatus = preferences.getBool("showShowcase");
//      int count = preferences.getInt("keyCountVisibility");
//
//      if (showcaseVisibilityStatus == null && count != 1) {
//        preferences.setInt("keyCountVisibility", 1);
//        preferences.setBool("showShowcase", false).then((bool success) {
//          if (success)
//            print("Successfull in writing showshoexase");
//          else
//            print("some bloody problem occured");
//        });
//        return true;
//      } else if(count == 2) {
//        return false;
//      }
//
//      preferences.setInt("keyCountVisibility", 2);
//      return true;
//    }

    displayShowcase() async {
      preferences = await SharedPreferences.getInstance();
      bool show = preferences.getBool("displayShow");
      if (show == null) {
        preferences.setBool('displayShow', false);
        return true;
      }
      return false;
    }

    displayShowcase().then((status) {
      if (status) {
        ShowCaseWidget.of(context).startShowCase([
          _one,
          _two,
          _three,
          _four,
          _five,
          _six,
          _seven,
          _eight,
        ]);
      }
    });


//    WidgetsBinding.instance.addPostFrameCallback((_) =>
//        ShowCaseWidget.of(context).startShowCase(
//            [_one, _two, _three, _four, _five, _six, _seven, _eight]));

    return Material(
      child: WillPopScope(
        onWillPop: _onBackPressed,
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            Container(
              child: Scaffold(
                appBar: AppBar(
                  title: Text("Google Explore"),
                  centerTitle: true,
                  backgroundColor: Colors.green,
                  actions: <Widget>[
                    Showcase(
                      key: _one,
                      description: 'Tap to see Loading options',
                      child: isLoading
                          ? IconButton(
                        icon: Icon(Icons.timer),
                        onPressed: () {},
                      )
                          : IconButton(
                        icon: Icon(Icons.refresh),
                        onPressed: () {
                          refresh();
                        },
                      ),
                    ),
//                  IconButton(
//                    icon: Icon(
//                      darkModeOn
//                          ? FontAwesomeIcons.lightbulb
//                          : FontAwesomeIcons.solidLightbulb,
//                      size: 18,
//                    ),
//                    onPressed: () {
//                      setState(() {
//                        darkModeOn = !darkModeOn;
//                      });
//                    },
//                  ),_
                    Showcase(
                      key: _two,
                      description: 'Tap to change Map view',
                      child: IconButton(
                        onPressed: _onMapTypeButtonPressed,
                        icon: Icon(
                          Icons.map,
                          size: 20,
                        ),
                      ),
                    ),
                    Showcase(
                      key: _three,
                      description: 'Tap to Share place',
                      child: IconButton(
                        onPressed: _onShareButtonPressed,
                        icon: Icon(
                          Icons.share,
                          size: 20,
                        ),
                      ),
                    ),
                  ],
                ),
                body: _body(),
//                floatingActionButton: Padding(
//                  padding: const EdgeInsets.fromLTRB(00.0, 0.0, 0.0, 80.0),
//                  child: Showcase(
//                    key: _seven,
//                    description: 'Tap to Created new card',
//                    shapeBorder: CircleBorder(),
//                    child: FloatingActionButton(
//                      child: Icon(
//                        Icons.add,
//                        color: Theme
//                            .of(context)
//                            .primaryColor,
//                      ),
//                      onPressed: () {
//                        addPlace();
//                      },
//                      backgroundColor: Colors.white,
//                    ),
//                  ),
//                ),
              ),
            ),
//            Padding(
//              padding: const EdgeInsets.fromLTRB(10.0, 200.0, 10.0, 10.0),
//              child: Align(
//                alignment: Alignment.topLeft,
//                child: Column(
//                  children: <Widget>[
//                  FloatingActionButton(
//                    onPressed: _onMapTypeButtonPressed,
//                    materialTapTargetSize: MaterialTapTargetSize.padded,
//                    backgroundColor: Colors.green,
//                    mini: true,
//                    child: const Icon(Icons.map),
//                  ),
//                  SizedBox(height: 10.0),
//                  FloatingActionButton(
//                    onPressed: _onAddMarkerButtonPressed,
//                    materialTapTargetSize: MaterialTapTargetSize.padded,
//                    backgroundColor: Colors.green,
//                    child: const Icon(Icons.add_location, size: 36.0),
//                  ),
//                  FloatingActionButton(
//                    mini: true,
//                    onPressed: () {
//                      showDialog(
//                          context: context,
//                          builder: (BuildContext context) =>
//                              locationDialog(mapController.cameraPosition, ''));
//                    },
//                    materialTapTargetSize: MaterialTapTargetSize.padded,
//                    backgroundColor: Colors.white10,
//                    child: const Icon(Icons.my_location, size: 36.0),
//                  ),
//                  ],
//                ),
//              ),
//            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 200.0, 10.0, 10.0),
              child: Align(
                alignment: Alignment.topRight,
                child: Column(
                  children: <Widget>[
                    Showcase(
                      key: _six,
                      description: 'Tap to see My location',
                      shapeBorder: CircleBorder(),
                      child: FloatingActionButton(
                        child: Icon(
                          Icons.gps_fixed,
                          color: Theme
                              .of(context)
                              .primaryColor,
                        ),
                        onPressed: getMyPosition,
                        backgroundColor: Colors.white,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Showcase(
                      key: _seven,
                      description: 'Tap to Created new card',
                      shapeBorder: CircleBorder(),
                      child: FloatingActionButton(
                        child: Icon(
                          Icons.add,
                          color: Theme
                              .of(context)
                              .primaryColor,
                        ),
                        onPressed: () {
                          addPlace();
                        },
                        backgroundColor: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 200.0, 10.0, 10.0),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Column(
                  children: <Widget>[
                    Showcase(
                      key: _five,
                      description: 'Tap to see Distance between points options',
                      shapeBorder: CircleBorder(),
                      child: FloatingActionButton(
                        child: Icon(
                          Icons.subdirectory_arrow_right,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          setState(() {
                            visibilityTag = true;
                            name = "My Location";
                          });
                        },
                        backgroundColor: Colors.blue,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 90.0,
              right: 10.0,
              left: 10.0,
              child: Showcase(
                key: _four,
                description: 'Tap to see Search location options',
                child: Container(
                  height: 50.0,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white),
                  child: new GestureDetector(
                    child: TextField(
                        decoration: InputDecoration(
                          hintText: name,
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(
                              left: 15.0, top: 15.0),
                          suffixIcon: IconButton(
                              onPressed: () {},
                              icon: Icon(Icons.search), iconSize: 30.0
                          ),
                        ),
                        onChanged: (val) {
                          setState(() {
                            searchAddr = val;
                            val = name;
                          });
                        },
                        onTap: openPlacesAutocomplete
                    ),
                  ),
                ),
              ),
            ),
            markersList && placesData != [] ? Positioned(
              bottom: 20.0,
              child: Container(
                height: 200.0,
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                child: PageView.builder(
                  controller: pageController,
                  itemCount: placesData.length,
                  itemBuilder: (BuildContext context, int index) {
                    print(index);
                    return _coffeeShopList(index);
                  },
                ),
              ),
            ) : new Container(),
            visibilityTag ? Positioned(
              top: 145.0,
              right: 10.0,
              left: 10.0,
              child: Container(
                height: 50.0,
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white),
                child: new GestureDetector(
                  child: TextField(
                      decoration: InputDecoration(
                        hintText: name2,
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.only(left: 15.0, top: 15.0),
                        suffixIcon: IconButton(
                            onPressed: openPlacesAutocomplete,
                            icon: Icon(Icons.search), iconSize: 30.0
                        ),
                      ),
                      onChanged: (val) {
                        setState(() {
                          searchAddr = val;
                          val = name;
                        });
                      },
                      onTap: openPlacesAutocomplete
                  ),
                ),
              ),
            ) : new Container(),
            SlidingUpPanel(
              maxHeight: MediaQuery
                  .of(context)
                  .size
                  .height * 0.8,
              minHeight: _panelHeightClosed,
              parallaxEnabled: true,
              backdropEnabled: true,
              backdropTapClosesPanel: true,
              parallaxOffset: .5,
              key: homeScaffoldKey,
              controller: _pc,
              panel: _panel(),
              collapsed: AnimatedContainer(
                duration: Duration(milliseconds: 500),
                decoration: BoxDecoration(
                  color: Colors.green[200],
                ),
                child: Showcase(
                  key: _eight,
                  description: 'Swipe up on the slider to see selected places',
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        height: 5.0,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width * 0.08,
                        decoration: BoxDecoration(
                          color: Colors.white60,
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
//                      CircleAvatar(
//                        radius: 15,
//                        backgroundColor: Colors.white,
//                        backgroundImage: CachedNetworkImageProvider(
//                            "https://miro.medium.com/max/800/1*gH1iKXJH8T12LIqhboZWEA.png"),
//                      ),
//                      RaisedButton(
//                        onPressed: () {
//                          setState(() {
//                            visibilityTag = true;
//                            name = "My Location";
//                          });
//                        },
//                        textColor: Colors.white,
//                        // padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
//                        color: Colors.blue,
//                        shape: RoundedRectangleBorder(
//                            borderRadius: new BorderRadius.circular(30.0)),
//                        child: Row(
//                          children: <Widget>[
//                            Text('Route', style: TextStyle(fontSize: 16)),
//                            Icon(Icons.navigate_next, color: Colors.white),
//                          ],
//                        ),
//                      ),
//                      SizedBox(
//                        width: 15,
//                      ),
                          Text(
                            "Locate Us",
                            textAlign: TextAlign.center,
                            style: Theme
                                .of(context)
                                .textTheme
                                .title,
                          ),
//                      IconButton(
//                        onPressed: () =>
//                            Share.share(
//                                "Download the new DevFest App and share with your tech friends.\nPlayStore -  http://bit.ly/2GDr18N"),
//                        icon: Icon(
//                          Icons.share,
//                          size: 20,
//                          color: Colors.white,
//                        ),
//                      ),
                        ],
                      ),
                      Offstage(),
                    ],
                  ),
                ),
              ),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(18.0),
                  topRight: Radius.circular(18.0)),
//            onPanelSlide: (double pos) =>
//                setState(() {
//                  _fabHeight = pos * (_panelHeightOpen - _panelHeightClosed) +
//                      _initFabHeight;
//                }),
            ),
          ],
        ),
      ),
    );
  }

  moveCamera() async {
    if(placesData != null) {
      var latLng = LatLng(
          placesData[pageController.page.toInt()].geometry.location.lat,
          placesData[pageController.page.toInt()].geometry.location.lng);

      mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: latLng,
          zoom: 17.0,
          bearing: 45.0,
          tilt: 45.0)));
    }
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(
              title: Text('Do you really want to exit the app?'),
              actions: <Widget>[
                FlatButton(
                  child: Text('No'),
                  onPressed: () => Navigator.pop(context, false),
                ),
                FlatButton(
                  child: Text('Yes'),
                  onPressed: () => Navigator.pop(context, true),
                )
              ],
            )
    );
  }

  openSettingsMenu(settingsName) async {
    try {
      resultSettingsOpening =
      await AccessSettingsMenu.openSettings(settingsType: settingsName);
    } catch (e) {
      resultSettingsOpening = false;
    }
  }

  Future _checkGps() async {
    if (!(await Geolocator().isLocationServiceEnabled())) {
      if (Theme
          .of(context)
          .platform == TargetPlatform.android) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Can't get gurrent location"),
              content:
              const Text('Please make sure you enable GPS and try again'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () async {
                    final AndroidIntent intent = new AndroidIntent(
                        action: 'android.settings.LOCATION_SOURCE_SETTINGS');
                    await intent.launch();
//                    openSettingsMenu("ACTION_WIRELESS_SETTINGS");
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    }
  }

  getPlaceId() {
    Geolocator()
        .placemarkFromCoordinates(userLocation.latitude, userLocation.longitude)
        .then((onValue) async {
      setState(() {
        if (onValue[0].thoroughfare != null) {
          searchAddr = onValue[0].thoroughfare;
        } else {
          searchAddr = onValue[0].subLocality;
        }
      });

      PlacesSearchResponse place = await _places.searchByText(searchAddr);
      if (mounted) {
        setState(() {
          this.isLoading = false;
          if (place.status == "OK") {
            this.place2 = place;
            name = place.results[0].name;
            id = place.results[0].placeId;
            fetchPlaceDetail(id);
          }
        });
      }
    });
  }

  void getNearbyPlaces(popular) async {
    setState(() {
      this.isLoading = true;
      this.errorMessage = null;
      placesData = [];
      mapController.clearMarkers();
    });
    final location = Location(lat, lng);
    int radius = int.parse(placeRadius);
    final result = await _places.searchNearbyWithRadius(location, radius);
    setState(() {
      this.isLoading = false;
      if (result.status == "OK") {
        places = result.results;
        result.results.forEach((f) {
          switch (popular) {
            case 'restaurant':
              if (f.types?.first == 'restaurant' || f.types?.first == 'bar' ||
                  f.types?.first == 'night_club' || f.types?.first == 'cafe') {
                placesData.add(f);
                final markerOptions = MarkerOptions(
                    position: LatLng(
                        f.geometry.location.lat, f.geometry.location.lng),
                    infoWindowText: InfoWindowText(
                        "${f.name}", "${f.types?.first}"));
                mapController.addMarker(markerOptions);
              }
              break;
            case 'hotel':
              if (f.types?.first == 'lodging') {
                placesData.add(f);
                final markerOptions = MarkerOptions(
                    position: LatLng(
                        f.geometry.location.lat, f.geometry.location.lng),
                    infoWindowText: InfoWindowText(
                        "${f.name}", "${f.types?.first}"));
                mapController.addMarker(markerOptions);
              }
              break;
            case 'store':
              if (f.types?.first == 'hardware_store' ||
                  f.types?.first == 'electronics_store' ||
                  f.types?.first == 'shopping_mail' ||
                  f.types?.first == 'departament_store' ||
                  f.types?.first == 'book_store' ||
                  f.types?.first == 'shoe_store') {
                placesData.add(f);
                final markerOptions = MarkerOptions(
                    position: LatLng(
                        f.geometry.location.lat, f.geometry.location.lng),
                    infoWindowText: InfoWindowText(
                        "${f.name}", "${f.types?.first}"));
                mapController.addMarker(markerOptions);
              }
              break;
            case 'interesting':
              if (f.types?.first == 'locality' || f.types?.first == 'museum' ||
                  f.types?.first == 'point_of_interest' ||
                  f.types?.first == 'zoo' ||
                  f.types?.first == 'park' || f.types?.first == 'library' ||
                  f.types?.first == 'aquarium' || f.types?.first == 'school' ||
                  f.types?.first == 'car_repair' || f.types?.first == 'gym' ||
                  f.types?.first == 'movie_theater') {
                placesData.add(f);
                final markerOptions = MarkerOptions(
                    position: LatLng(
                        f.geometry.location.lat, f.geometry.location.lng),
                    infoWindowText: InfoWindowText(
                        "${f.name}", "${f.types?.first}"));
                mapController.addMarker(markerOptions);
              }
              break;
            case 'bank':
              if (f.types?.first == 'bank' || f.types?.first == 'finance') {
                placesData.add(f);
                final markerOptions = MarkerOptions(
                    position: LatLng(
                        f.geometry.location.lat, f.geometry.location.lng),
                    infoWindowText: InfoWindowText(
                        "${f.name}", "${f.types?.first}"));
                mapController.addMarker(markerOptions);
              }
              break;
            case 'hospital':
              if (f.types?.first == 'hospital' || f.types?.first == 'health' ||
                  f.types?.first == 'doctor') {
                placesData.add(f);
                final markerOptions = MarkerOptions(
                    position: LatLng(
                        f.geometry.location.lat, f.geometry.location.lng),
                    infoWindowText: InfoWindowText(
                        "${f.name}", "${f.types?.first}"));
                mapController.addMarker(markerOptions);
              }
              break;
            case '':
              placesData.add(f);
              final markerOptions = MarkerOptions(
                  position: LatLng(
                      f.geometry.location.lat, f.geometry.location.lng),
                  infoWindowText: InfoWindowText(
                      "${f.name}", "${f.types?.first}"));
              mapController.addMarker(markerOptions);
              break;
            default:
              break;
          }
        });
      }
    });
  }

  Future openPlacesAutocomplete() async {
    showDetailPlace2();
//    searchAndNavigate();
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: kGoogleApiKey,
      mode: Mode.fullscreen, // Mode.fullscreen
      language: "en",
    );
    setState(() {
      if (p.description != null) {
        name = p.description;
        markersList = false;
      }
    });
    displayPrediction(p);
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  void fetchPlaceDetail(pId) async {
    PlacesDetailsResponse place = await _places.getDetailsByPlaceId(pId);
    if (mounted) {
      setState(() {
        this.isLoading = false;
        if (place.status == "OK") {
          this.place = place;
        }
      });
    }
  }

  getMyPosition() {
    setState(() {
      markersList = false;
    });
    mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(userLocation.latitude, userLocation.longitude),
        // bearing: 90.0
        zoom: 16.0,
        tilt: 30.0)));
    getPlaceId();
  }

  Future<Null> displayPrediction(Prediction p) async {
    if (p != null) {
      PlacesDetailsResponse detail =
      await _places.getDetailsByPlaceId(p.placeId);
      setState(() {
        pId = p.placeId;
      });
      setState(() {
        this.lat = detail.result.geometry.location.lat;
        this.lng = detail.result.geometry.location.lng;
        if (visibilityTag) {
          whereTo = [this.lat, this.lng];
        }
        name = detail.result.name;
      });

      double lat = detail.result.geometry.location.lat;
      double lng = detail.result.geometry.location.lng;

      mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: LatLng(lat, lng),
          // bearing: 90.0
          zoom: 16.0,
          tilt: 30.0)));
      fetchPlaceDetail(pId);
      Navigator.pop(context);
      timer = Timer(
          Duration(seconds: 3), () => _onAddMarkerButtonPressed());
      _onAddMarkerButtonPressed();
//      var address = await Geocoder.local.findAddressesFromQuery(p.description);
      if (whereTo != []) {
        showDistance();
      }
    }
  }

  Future<Null> showDetailPlace(String placeId) async {
    setState(() {
      pId = placeId;
    });
    showDetailPlace2();
    if (placeId != null) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => PlaceDetailWidget(placeId)),
      );
    }
    Navigator.pop(context);
  }

  Future<Null> showDetailPlace2() async {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => PlaceDetailWidget(pId)),
    );
  }

  Future<Position> _getLocation() async {
    var currentLocation;
    try {
      currentLocation = await geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.best);
    } catch (e) {
      currentLocation = null;
    }
    return currentLocation;
  }

  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.hybrid
          : MapType.normal;
      mapController
          .updateMapOptions(GoogleMapOptions(mapType: _currentMapType));
    });
  }

  void _onAddMarkerButtonPressed() async {
    // getNearbyPlaces();
    if (placeDetail != null) {
      await mapController.addMarker(
        MarkerOptions(
            position: LatLng(
              mapController.cameraPosition.target.latitude,
              mapController.cameraPosition.target.longitude,
            ),
            infoWindowText: InfoWindowText(
                "${placeDetail.name}", "${placeDetail.formattedAddress}")),
      );
    }
    setState(() {
      whereFrom = [
        mapController.cameraPosition.target.latitude,
        mapController.cameraPosition.target.longitude
      ];
    });
//    showDialog(
//        context: context,
//        builder: (BuildContext context) =>
//            locationDialog(mapController.cameraPosition, (placeDetail.name)));
  }

  Widget locationDialog(CameraPosition pos, name) {
    return CupertinoAlertDialog(
      title: new Text("Current Location"),
      content: Column(
        children: [
          SizedBox(height: 10.0),
          Text(name, textAlign: TextAlign.center),
          Text('lat : ' +
              pos.target.latitude.toDouble().toStringAsFixed(6)),
          Text('lng: ' +
              pos.target.longitude.toDouble().toStringAsFixed(6))
        ],
      ),
      actions: <Widget>[
        new CupertinoDialogAction(
            child: const Text('Where To'),
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pop(context, 'Discard');
              openPlacesAutocomplete();
            }),
        new CupertinoDialogAction(
            child: const Text('Cancel'),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context, 'Cancel');
            }),
      ],
    );
  }

  Widget _panel() {
    Widget expandedChild;

    if (isLoading) {
      expandedChild = Center(child: CircularProgressIndicator(value: null));
    } else if (markersList) {
      expandedChild = buildPlacesList();
    }
    if (place == null) {
      return _textNotPlace();
    }

    placeDetail = place.result;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[

        SizedBox(
          height: 12.0,
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 30,
              height: 5,
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.all(Radius.circular(12.0))),
            ),
          ],
        ),

        SizedBox(
          height: 8.0,
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Explore $name",
              style: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 18.0,
              ),
            ),
          ],
        ),

        SizedBox(
          height: 20.0,
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(4.0),
                  child: IconButton(
                    onPressed: () {
                      setState(() {
                        markersList = true;
                        popularPlace = '';
                      });
                      _placeRadiusDialog(context);
                      _pc.close();
                      mapController.animateCamera(
                          CameraUpdate.newCameraPosition(CameraPosition(
                              target: LatLng(lat, lng),
                              // bearing: 90.0
                              zoom: 12.0,
                              tilt: 30.0)));
                    },
                    icon: Icon(
                      Icons.favorite,
                      color: Colors.white,
                    ),
                  ),
                  decoration:
                  BoxDecoration(
                      color: Colors.blue, shape: BoxShape.circle, boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15),
                      blurRadius: 8.0,
                    )
                  ]),
                ),
                SizedBox(
                  height: 12.0,
                ),
                Text("Popular"),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(4.0),
                  child: IconButton(
                    onPressed: () {
                      setState(() {
                        markersList = true;
                        popularPlace = 'restaurant';
                      });
                      getNearbyPlaces('restaurant');
                      _pc.close();
                      mapController.animateCamera(
                          CameraUpdate.newCameraPosition(CameraPosition(
                              target: LatLng(lat, lng),
                              // bearing: 90.0
                              zoom: 13.0,
                              tilt: 30.0)));
                    },
                    icon: Icon(
                      Icons.restaurant,
                      color: Colors.white,
                    ),
                  ),
                  decoration:
                  BoxDecoration(
                      color: Colors.deepOrange,
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.15),
                          blurRadius: 8.0,
                        )
                      ]),
                ),
                SizedBox(
                  height: 12.0,
                ),
                Text("Food"),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(4.0),
                  child: IconButton(
                    onPressed: () {
                      setState(() {
                        markersList = true;
                        popularPlace = 'hotel';
                      });
                      getNearbyPlaces('hotel');
                      _pc.close();
                      mapController.animateCamera(
                          CameraUpdate.newCameraPosition(CameraPosition(
                              target: LatLng(lat, lng),
                              // bearing: 90.0
                              zoom: 13.0,
                              tilt: 30.0)));
                    },
                    icon: Icon(
                      Icons.hotel,
                      color: Colors.white,
                    ),
                  ),
                  decoration:
                  BoxDecoration(
                      color: Colors.amber, shape: BoxShape.circle, boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15),
                      blurRadius: 8.0,
                    )
                  ]),
                ),
                SizedBox(
                  height: 12.0,
                ),
                Text("Hotel"),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(4.0),
                  child: IconButton(
                    onPressed: () {
                      setState(() {
                        markersList = true;
                        popularPlace = 'store';
                      });
                      getNearbyPlaces('store');
                      _pc.close();
                      mapController.animateCamera(
                          CameraUpdate.newCameraPosition(CameraPosition(
                              target: LatLng(lat, lng),
                              // bearing: 90.0
                              zoom: 13.0,
                              tilt: 30.0)));
                    },
                    icon: Icon(
                      Icons.local_mall,
                      color: Colors.white,
                    ),
                  ),
                  decoration:
                  BoxDecoration(
                      color: Colors.green, shape: BoxShape.circle, boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15),
                      blurRadius: 8.0,
                    )
                  ]),
                ),
                SizedBox(
                  height: 12.0,
                ),
                Text("Store"),
              ],
            ),
//            _button("Events", Icons.event, Colors.amber),
//            _button("More", Icons.more_horiz, Colors.green),
          ],
        ),

        SizedBox(
          height: 20.0,
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(4.0),
                  child: IconButton(
                    onPressed: () {
                      setState(() {
                        markersList = true;
                        popularPlace = 'interesting';
                      });
                      getNearbyPlaces('interesting');
                      _pc.close();
                      mapController.animateCamera(
                          CameraUpdate.newCameraPosition(CameraPosition(
                              target: LatLng(lat, lng),
                              // bearing: 90.0
                              zoom: 13.0,
                              tilt: 30.0)));
                    },
                    icon: Icon(
                      Icons.place,
                      color: Colors.white,
                    ),
                  ),
                  decoration:
                  BoxDecoration(
                      color: Colors.orange, shape: BoxShape.circle, boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15),
                      blurRadius: 8.0,
                    )
                  ]),
                ),
                SizedBox(
                  height: 12.0,
                ),
                Text("Interest"),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(4.0),
                  child: IconButton(
                    onPressed: () {
                      setState(() {
                        markersList = true;
                        popularPlace = 'bank';
                      });
                      getNearbyPlaces('bank');
                      _pc.close();
                      mapController.animateCamera(
                          CameraUpdate.newCameraPosition(CameraPosition(
                              target: LatLng(lat, lng),
                              // bearing: 90.0
                              zoom: 13.0,
                              tilt: 30.0)));
                    },
                    icon: Icon(
                      Icons.account_balance,
                      color: Colors.white,
                    ),
                  ),
                  decoration:
                  BoxDecoration(
                      color: Colors.deepPurple,
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.15),
                          blurRadius: 8.0,
                        )
                      ]),
                ),
                SizedBox(
                  height: 12.0,
                ),
                Text("Bank"),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(4.0),
                  child: IconButton(
                    onPressed: () {
                      setState(() {
                        markersList = true;
                        popularPlace = 'hospital';
                      });
                      getNearbyPlaces('hospital');
                      _pc.close();
                      mapController.animateCamera(
                          CameraUpdate.newCameraPosition(CameraPosition(
                              target: LatLng(lat, lng),
                              // bearing: 90.0
                              zoom: 13.0,
                              tilt: 30.0)));
                    },
                    icon: Icon(
                      Icons.local_hospital,
                      color: Colors.white,
                    ),
                  ),
                  decoration:
                  BoxDecoration(
                      color: Colors.red, shape: BoxShape.circle, boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15),
                      blurRadius: 8.0,
                    )
                  ]),
                ),
                SizedBox(
                  height: 12.0,
                ),
                Text("Hospital"),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(4.0),
                  child: IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.more_horiz,
                      color: Colors.white,
                    ),
                  ),
                  decoration:
                  BoxDecoration(
                      color: Colors.grey, shape: BoxShape.circle, boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15),
                      blurRadius: 8.0,
                    )
                  ]),
                ),
                SizedBox(
                  height: 12.0,
                ),
                Text("More"),
              ],
            ),
          ],
        ),

        SizedBox(
          height: 20.0,
        ),

        Expanded(
            child: !markersList
                ? buildPlaceDetailList(placeDetail)
                : expandedChild
        ),

        SizedBox(
          height: 20.0,
        ),

//        Container(
//          padding: const EdgeInsets.only(left: 24.0, right: 24.0),
//          child: Column(
//            crossAxisAlignment: CrossAxisAlignment.start,
//            children: <Widget>[
//
//              Text("Images", style: TextStyle(fontWeight: FontWeight.w600,)),
//
//              SizedBox(height: 12.0,),
//
//              Row(
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                children: <Widget>[
//
//                  CachedNetworkImage(
//                    imageUrl: "http://www.oryxitourism.com/wp-content/uploads/2016/08/Armenia-Tour.jpg",
//                    height: 120.0,
//                    width: (MediaQuery
//                        .of(context)
//                        .size
//                        .width - 48) / 2 - 2,
//                    fit: BoxFit.cover,
//                  ),
//
//                  CachedNetworkImage(
//                    imageUrl: "https://images.squarespace-cdn.com/content/v1/57b9b98a29687f1ef5c622df/1472654514146-SSSPYPQAVO1F9URTI04N/ke17ZwdGBToddI8pDm48kBbOjajeQQtePfd1O4jqnaAUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcXMwU3bcPXQlGfZeAHgJ5LFKd7rsAsrvG_OCzJm9yN6vMX6yVKuxxP6-raXwpph8G/republic+square+yerevan?format=1500w",
//                    width: (MediaQuery
//                        .of(context)
//                        .size
//                        .width - 48) / 2 - 2,
//                    height: 120.0,
//                    fit: BoxFit.cover,
//                  ),
//
//                ],
//              ),
//            ],
//          ),
//        ),

//        SizedBox(height: 36.0,),
//
//        Container(
//          padding: const EdgeInsets.only(left: 24.0, right: 24.0),
//          child: Column(
//            crossAxisAlignment: CrossAxisAlignment.start,
//
//            children: <Widget>[
//              Text(
//                  "About",
//                  style: TextStyle(fontWeight: FontWeight.w600,)),
//
//              SizedBox(height: 12.0,),
//
//              Text(
//                "Yerevan (/jɛrəˈvɑːn/ YERR-ə-VAHN; Armenian: Երևան[a] [jɛɾɛˈvɑn] (About this soundlisten),"
//                    " sometimes spelled Erevan)[b] is the capital and largest city of Armenia as well as one of"
//                    " the world's oldest continuously inhabited cities.[17] Situated along the Hrazdan River,"
//                    " Yerevan is the administrative, cultural, and industrial center of the country."
//                    " It has been the capital since 1918, the fourteenth in the history of Armenia and "
//                    "the seventh located in or around the Ararat plain.",
//                maxLines: 6,
//                overflow: TextOverflow.ellipsis,
//              ),
//            ],
//          ),
//        ),

      ],
    );
  }

  Widget _textNotPlace() {
    return Center(
      child: Container(
        child: Text(
          "Information is absent",
          style: TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 18.0,
          ),
        ),
      ),
    );
  }

  ListView buildPlacesList() {
    List<Widget> list;
    final placesWidget = placesData.map((f) {
      list = [
        Padding(
          padding: EdgeInsets.only(bottom: 4.0),
          child: Text(
            f.name,
            style: Theme
                .of(context)
                .textTheme
                .subtitle,
          ),
        )
      ];

      if (f.formattedAddress != null) {
        list.add(Padding(
          padding: EdgeInsets.only(bottom: 2.0),
          child: Text(
            f.formattedAddress,
            style: Theme
                .of(context)
                .textTheme
                .subtitle,
          ),
        ));
      }

      if (f.vicinity != null) {
        list.add(Padding(
          padding: EdgeInsets.only(bottom: 2.0),
          child: Text(
            f.vicinity,
            style: Theme
                .of(context)
                .textTheme
                .body1,
          ),
        ));
      }

      if (f.types?.first != null) {
        list.add(Padding(
          padding: EdgeInsets.only(bottom: 2.0),
          child: Text(
            f.types.first,
            style: Theme
                .of(context)
                .textTheme
                .caption,
          ),
        ));
      }

      return Padding(
        padding: EdgeInsets.only(
            top: 4.0, bottom: 4.0, left: 8.0, right: 8.0),
        child: Card(
          child: InkWell(
            onTap: () {
              _pc.close();
              showDetailPlace(f.placeId);
            },
            highlightColor: Colors.lightBlueAccent,
            splashColor: Colors.red,
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: list,
              ),
            ),
          ),
        ),
      );
    }).toList();

    return ListView(shrinkWrap: true, children: placesWidget);
  }

  searchAndNavigate() {
    Geolocator().placemarkFromAddress(searchAddr).then((result) {
      setState(() {
        if (result[0].administrativeArea == "") {
          name = result[0].name;
        } else {
          name = result[0].administrativeArea;
        }
      });

      mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target:
          LatLng(result[0].position.latitude, result[0].position.longitude),
          zoom: 16.0,
          tilt: 30.0)));
    });
  }

  Widget locationDistanceDialog(dist) {
    return CupertinoAlertDialog(
      title: new Text("Distance"),
      content: Column(
        children: [
          SizedBox(height: 10.0),
          Text("$dist km", textAlign: TextAlign.center),
        ],
      ),
      actions: <Widget>[
        new CupertinoDialogAction(
            child: const Text('Where To'),
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pop(context, 'Discard');
            }),
        new CupertinoDialogAction(
            child: const Text('Cancel'),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context, 'Cancel');
            }),
      ],
    );
  }

  Future<String> _placeRadiusDialog(BuildContext context) async {
    return showDialog<String>(
      context: context,
      barrierDismissible: false,
      // dialog is dismissible with a tap on the barrier
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Enter search radius'),
          content: new Row(
            children: <Widget>[
              new Expanded(
                  child: new TextField(
                    autofocus: true,
                    decoration: new InputDecoration(
                      labelText: 'Radius',
                      hintText: placeRadius,
                    ),
                    keyboardType: TextInputType.phone,
                    onChanged: (value) {
                      setState(() {
                        placeRadius = value;
                      });
                    },
                  ))
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                getNearbyPlaces('');
                Navigator.of(context).pop(placeRadius);
              },
            ),
          ],
        );
      },
    );
  }

  showDistance() {
    setState(() {
      visibilityTag = false;
    });

    if (whereFrom.length > 0 && whereTo.length > 0) {
      Geolocator().distanceBetween(
          whereFrom[0], whereFrom[1], whereTo[0], whereTo[1]).then((dist) =>
      {
        distance = ((dist / 1000).toStringAsFixed(1)),
        showDialog(context: context, builder: (BuildContext context) =>
            locationDistanceDialog(distance))
      });
    }

//    ccc = new List();
//    ccc = [whereFrom[0], whereFrom[1], whereTo[0], whereTo[1]];
//
//    mapView.onMapReady.listen((_) {
//      List<Polyline> _lines  = <Polyline>[(new Polyline("12", ccc, width: 15.0))];
//      mapView.setPolylines(_lines);
//    });


  }

  String buildPhotoURL(String photoReference) {
    return "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$photoReference&key=$kGoogleApiKey";
  }

  ListView buildPlaceDetailList(PlaceDetails placeDetail) {
    List<Widget> list = [];
    if (placeDetail.photos != null) {
      final photos = placeDetail.photos;

      image = photos[0].photoReference;
      for (var x = 0; x < photos.length; x++) {
        imagesList.add(photos[x].photoReference);
      }

      list.add(SizedBox(
          height: 150.0,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: photos.length,
              itemBuilder: (context, index) {
                cardIndex = index;
                return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 5.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      child: SizedBox(
                        height: 150,
                        child: Image.network(
                            buildPhotoURL(photos[index].photoReference)),
                      ),
                    ));
              })));
    }

    list.add(
      Padding(
          padding:
          EdgeInsets.only(top: 4.0, left: 8.0, right: 8.0, bottom: 4.0),
          child: Text(
            placeDetail.name,
            style: Theme
                .of(context)
                .textTheme
                .subtitle,
          )),
    );

    if (placeDetail.formattedAddress != null) {
      titleText = placeDetail.formattedAddress;
      list.add(
        Padding(
            padding:
            EdgeInsets.only(top: 4.0, left: 8.0, right: 8.0, bottom: 4.0),
            child: Text(
              placeDetail.formattedAddress,
              style: Theme
                  .of(context)
                  .textTheme
                  .body1,
            )),
      );
    }

    if (placeDetail.types?.first != null) {
      list.add(
        Padding(
            padding:
            EdgeInsets.only(top: 4.0, left: 8.0, right: 8.0, bottom: 0.0),
            child: Text(
              placeDetail.types.first.toUpperCase(),
              style: Theme
                  .of(context)
                  .textTheme
                  .caption,
            )),
      );
    }

    if (placeDetail.formattedPhoneNumber != null) {
      phoneNumber = placeDetail.formattedPhoneNumber;
      list.add(
        Padding(
            padding:
            EdgeInsets.only(top: 4.0, left: 8.0, right: 8.0, bottom: 4.0),
            child: Text(
              placeDetail.formattedPhoneNumber,
              style: Theme
                  .of(context)
                  .textTheme
                  .button,
            )),
      );
    }

    if (placeDetail.openingHours != null) {
      final openingHour = placeDetail.openingHours;
      var text = '';
      if (openingHour.openNow) {
        text = 'Opening Now';
      } else {
        text = 'Closed';
      }
      list.add(
        Padding(
            padding:
            EdgeInsets.only(top: 0.0, left: 8.0, right: 8.0, bottom: 4.0),
            child: Text(
              text,
              style: Theme
                  .of(context)
                  .textTheme
                  .caption,
            )),
      );
    }

    if (placeDetail.website != null) {
      website = placeDetail.website;
      list.add(
        Padding(
          padding:
          EdgeInsets.only(top: 0.0, left: 8.0, right: 8.0, bottom: 4.0),
          child: InkWell(
            child: Text(
              "Website: ${placeDetail.website}",
              style: Theme
                  .of(context)
                  .textTheme
                  .caption,
            ),
            onTap: () async {
              if (await canLaunch(placeDetail.website)) {
                await launch(placeDetail.website);
              } else {
                throw 'Could not launch ${placeDetail.website}';
              }
            },
          ),
        ),
      );
    }

    if (placeDetail.rating != null) {
      list.add(
        Padding(
            padding:
            EdgeInsets.only(top: 0.0, left: 8.0, right: 8.0, bottom: 4.0),
            child: Text(
              "Rating: ${placeDetail.rating}",
              style: Theme
                  .of(context)
                  .textTheme
                  .caption,
            )),
      );
    }

    if (placeDetail.url != null) {
      url = placeDetail.url;
      list.add(
        Padding(
          padding:
          EdgeInsets.only(top: 0.0, left: 8.0, right: 8.0, bottom: 4.0),
          child: InkWell(
            child: Text(
              "More info: ${placeDetail.url}",
              style: Theme
                  .of(context)
                  .textTheme
                  .caption,
            ),
            onTap: () async {
              if (await canLaunch(placeDetail.url)) {
                await launch(placeDetail.url);
              } else {
                throw 'Could not launch ${placeDetail.url}';
              }
            },
          ),
        ),
      );
    }

    return ListView(
      shrinkWrap: true,
      children: list,
    );
  }

  addPlace() async {
    final prefs = await SharedPreferences.getInstance();

    final keyImg = 'my_img_key';
    final keyImages = 'my_images_key';
    final keyTitle = 'my_title_key';
    // final keyWebsite = 'my_website_key';
    // final keyPhoneNumber = 'my_phoneNumber_key';
    // final keyUrl = 'my_url_key';
    final key1 = 'dateImage';
    final key2 = 'dateImages';
    final key3 = 'dateTitle';
    // final key4 = 'dateWebsite';
    // final key5 = 'dateNumber';
    // final key6 = 'dateUrl';

    if (image == null) {
      _snackBar('Favorite place no images!', Colors.red);
      return;
    }
    setState(() {
      prefs.setStringList(keyImg, [image]);
      prefs.setStringList(keyImages, [pId]);
      prefs.setStringList(keyTitle, [titleText]);
//      prefs.setStringList(keyWebsite, [website]);
//      prefs.setStringList(keyPhoneNumber, [phoneNumber]);
//      prefs.setStringList(keyUrl, [url]);

      var myStringList = prefs.getStringList(keyImg) ?? [];
      var myStringList2 = prefs.getStringList(keyImages) ?? [];
      var myStringList3 = prefs.getStringList(keyTitle) ?? [];
//      final myStringList4 = prefs.getStringList(keyWebsite) ?? [];
//      final myStringList5 = prefs.getStringList(keyPhoneNumber) ?? [];
//      final myStringList6 = prefs.getStringList(keyUrl) ?? [];
      if (myStringList3 == [] || myStringList == []) {
        _snackBar('Missing picture or text!', Colors.red);
      } else {
        data.add(myStringList2[0]);
        images.add(buildPhotoURL(myStringList[0]));
        title.add(myStringList3[0]);
//      websites.add(myStringList4[0]);
//      phonesNumber.add(myStringList5[0]);
//      urls.add(myStringList6[0]);

        prefs.setStringList(key1, images);
        prefs.setStringList(key2, data);
        prefs.setStringList(key3, title);
//      prefs.setStringList(key4, websites);
//      prefs.setStringList(key5, phonesNumber);
//      prefs.setStringList(key6, urls);
        _snackBar('Favorite place created!', Colors.green);
      }
    });
  }

  Future<Null> _onShareButtonPressed() async {
    if (placeDetail != null && placeDetail.formattedAddress != null) {
      final RenderBox box = context.findRenderObject();
      Share.share(placeDetail.formattedAddress,
          subject: placeDetail.name,
          sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    }
  }

  void refresh() async {
    var timer = Timer(Duration(seconds: 2), () =>
    {
      setState(() {
        this.isLoading = true;
        this.errorMessage = null;
      })
    });


    _getLocation().then((position) {
      userLocation = position;
      setState(() {
        lat = userLocation.latitude;
        lng = userLocation.longitude;
      });
      getPlaceId();
      getMyPosition();
    });
    getMyPosition();
    fetchPlaceDetail(pId);
    timer.cancel();
    setState(() {
      this.isLoading = false;
    });
  }

//  Widget _button(String label, IconData icon, Color color) {
//    return Column(
//      children: <Widget>[
//        Container(
//          padding: const EdgeInsets.all(16.0),
//          child: Icon(
//            icon,
//            color: Colors.white,
//          ),
//          decoration:
//          BoxDecoration(color: color, shape: BoxShape.circle, boxShadow: [
//            BoxShadow(
//              color: Color.fromRGBO(0, 0, 0, 0.15),
//              blurRadius: 8.0,
//            )
//          ]),
//        ),
//        SizedBox(
//          height: 12.0,
//        ),
//        Text(label),
//      ],
//    );
//  }

  Widget _body() {
    return GoogleMap(
      onMapCreated: _onMapCreated,
      options: GoogleMapOptions(
        scrollGesturesEnabled: true,
        tiltGesturesEnabled: true,
        rotateGesturesEnabled: true,
        myLocationEnabled: true,
        compassEnabled: true,
        mapType: MapType.normal,
        trackCameraPosition: true,
        zoomGesturesEnabled: true,
      ),
    );
  }

  _snackBar(messageText, color) {
    final snackBar = SnackBar(
      content: Text(messageText,
          style: TextStyle(
            color: Colors.white,
            fontSize: 23.0,
            fontFamily: "Calibre-Semibold",
            letterSpacing: 1.0,
          )),
      backgroundColor: color,
      action: SnackBarAction(
        label: 'show',
        textColor: Colors.white,
        onPressed: () {},
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    if (place != null) {
      final placeDetail = place.result;
      final location = place.result.geometry.location;
      final lat = location.lat;
      final lng = location.lng;
      final center = LatLng(lat, lng);
      var markerOptions = MarkerOptions(
          position: center,
          infoWindowText: InfoWindowText(
              "${placeDetail.name}", "${placeDetail.formattedAddress}"));
      mapController.addMarker(markerOptions);
      mapController.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(target: center, zoom: 16.0)));
    }
    // refresh();
  }

//  void initPlatformState() async {
//    Map<String, double> my_location;
//    try {
//      my_location = (await location.getLocation()) as Map<String, double>;
//      error = "";
//    } on PlatformException catch (c) {
//      if (c.code == "PERMISSION_DENIED")
//        error = "Perpission Denied";
//      else if (c.code == "PERMISSION_DENIED_NEVER_ASK")
//        error =
//        "Permission denied - please ask the user to enable it from the app settings";
//      my_location = null;
//    }
//
//    print(currentLocation);
//    setState(() {
//      currentLocation = my_location;
//    });
//  }

}






