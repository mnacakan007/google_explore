//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:google_maps_flutter/google_maps_flutter.dart';
//
//import 'map.dart';
//
//
//void onScroll() {
//  if (MapsState().pageController.page.toInt() != MapsState().prevPage) {
//    MapsState().prevPage = MapsState().pageController.page.toInt();
//    moveCamera();
//  }
//}
//
//coffeeShopList(index) {
//
//  return AnimatedBuilder(
//    animation: MapsState().pageController,
//    builder: (BuildContext context, Widget widget) {
//      double value = 1;
//      if (MapsState().pageController.position.haveDimensions) {
//        value = MapsState().pageController.page - index;
//        value = (1 - (value.abs() * 0.3) + 0.06).clamp(0.0, 1.0);
//      }
//      return Center(
//        child: SizedBox(
//          height: Curves.easeInOut.transform(value) * 125.0,
//          width: Curves.easeInOut.transform(value) * 350.0,
//          child: widget,
//        ),
//      );
//    },
//    child: InkWell(
//        onTap: () {
//          moveCamera();
//        },
//        child: Stack(children: [
//          Center(
//              child: Container(
//                  margin: EdgeInsets.symmetric(
//                    horizontal: 10.0,
//                    vertical: 20.0,
//                  ),
//                  height: 125.0,
//                  width: 275.0,
//                  decoration: BoxDecoration(
//                      borderRadius: BorderRadius.circular(10.0),
//                      boxShadow: [
//                        BoxShadow(
//                          color: Colors.black54,
//                          offset: Offset(0.0, 4.0),
//                          blurRadius: 10.0,
//                        ),
//                      ]),
//                  child: Container(
//                      decoration: BoxDecoration(
//                          borderRadius: BorderRadius.circular(10.0),
//                          color: Colors.white),
//                      child: Row(children: [
//                        MapsState().placesData != [] ? Container(
//                            height: 90.0,
//                            width: 90.0,
//                            decoration: BoxDecoration(
//                                borderRadius: BorderRadius.only(
//                                    bottomLeft: Radius.circular(10.0),
//                                    topLeft: Radius.circular(10.0)),
//                                image: DecorationImage(
//                                    image: NetworkImage(
//                                        MapsState().buildPhotoURL(MapsState().placesData[index]?.photos == null ? 'CmRaAAAASpwaTDoRcKz3t89e7gZ53jQMVG1LKb-OI7EwZZwbINt5IEiShHFFbFGraquLtDeqe_as0yKbZBLrx2kIIbXwWiN5vUNvhARMcyIYOIBm0rnE60SCKea89MZTITMjDYiVEhBHpnC__xoAuTVkOnr1pe53GhQpO2DeCTqq39-QZQJ_LFY_4Lr3ZQ' : MapsState().placesData[index].photos[0].photoReference)),
//                                    fit: BoxFit.cover))) : Container(),
//                        SizedBox(width: 5.0),
//                        Column(
//                            mainAxisAlignment: MainAxisAlignment.spaceAround,
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: [
//                              Container(
//                                width: 170.0,
//                                child: Text(
//                                  MapsState().placesData[index].name ?? '',
//                                  style: TextStyle(
//                                      fontSize: 12.5,
//                                      fontWeight: FontWeight.bold),
//                                ),
//                              ),
//                              Container(
//                                width: 170.0,
//                                child: Text(
//                                  MapsState().placesData[index].types.first ?? '',
//                                  style: TextStyle(
//                                      fontSize: 11.0,
//                                      fontWeight: FontWeight.w300),
//                                ),
//                              ),
//                              Container(
//                                width: 170.0,
//                                child: Text(
//                                  MapsState().placesData[index].formattedAddress ?? '',
//                                  style: TextStyle(
//                                      fontSize: 12.0,
//                                      fontWeight: FontWeight.w600),
//                                ),
//                              ),
//                              Container(
//                                width: 170.0,
//                                child: Text(
//                                  MapsState().placesData[index].vicinity ?? '',
//                                  style: TextStyle(
//                                      fontSize: 11.0,
//                                      fontWeight: FontWeight.w300),
//                                ),
//                              ),
//                            ])
//                      ]))))
//        ])),
//  );
//}
//
//moveCamera() async {
//  if(MapsState().placesData != null) {
//    var latLng = LatLng(
//        MapsState().placesData[MapsState().pageController.page.toInt()].geometry.location.lat,
//        MapsState().placesData[MapsState().pageController.page.toInt()].geometry.location.lng);
//
//    MapsState().mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
//        target: latLng,
//        zoom: 17.0,
//        bearing: 45.0,
//        tilt: 45.0)));
//  }
//}