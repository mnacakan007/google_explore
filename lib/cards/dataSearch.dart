import 'package:flutter/material.dart';
import 'package:google_search/cards/utils.dart';
import 'package:google_search/home/home.dart';

import 'cardPage.dart';
import 'data.dart';

class DataSearch extends SearchDelegate<String> {
  int imageIndex = 0;
  bool isExpanded = false;
  int currentSizeIndex = 0;
  int currentColorIndex = 0;

  @override
  List<Widget> buildActions(BuildContext context) {
    return [IconButton(icon: Icon(Icons.clear), onPressed: () {
      query = "";
    },)
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation,
        ),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    return Center(
      child: Container(
        padding: const EdgeInsets.only(
            left: 20.0, bottom: 100.0, right: 20.0, top: 15.0),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(16.0),
          child: Container(
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(3.0, 6.0),
                  blurRadius: 10.0)
            ]),
            child: AspectRatio(
              aspectRatio: cardAspectRatio,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Image.network(images[imageIndex], fit: BoxFit.cover),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
//                        PopupMenuButton(
//                          onSelected: (dynamic val) async {
//                            print("val $val");
//                            final prefs = await SharedPreferences.getInstance();
//                            dynamic myStringList = prefs.getStringList('dateImage');
//                            myStringList.edit().remove(val).commit();
//                            //images.removeWhere((data) => data == val);
//                          },
//                          icon: Icon(Icons.menu),
//                          itemBuilder: (context) => [
//                            PopupMenuItem(
//                              value: images[imageIndex],
//                              child: Text("Delete"),
//                            ),
//                          ],
//                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 16.0, vertical: 8.0),
                          child: Text(title[imageIndex],
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 25.0,
                                  fontFamily: "SF-Pro-Text-Regular")),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: screenAwareSize(16.0, context),
                              right: screenAwareSize(16.0, context)),
                          child: AnimatedCrossFade(
                            firstChild: Text(
                              desc,
                              maxLines: 2,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: screenAwareSize(12.0, context),
                                  fontFamily: "Montserrat-Medium"),
                            ),
                            secondChild: Text(
                              desc,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: screenAwareSize(12.0, context),
                                  fontFamily: "Montserrat-Medium"),
                            ),
                            crossFadeState: isExpanded
                                ? CrossFadeState.showSecond
                                : CrossFadeState.showFirst,
                            duration: kThemeAnimationDuration,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: screenAwareSize(16.0, context),
                              right: screenAwareSize(16.0, context)),
                          child: GestureDetector(
                              onTap: () {
                                isExpanded ? isExpanded = false : isExpanded =
                                true;
                              },
                              child: Text(
                                isExpanded ? "less" : "more..",
                                style: TextStyle(
                                    color: Color(0xFFFB382F),
                                    fontWeight: FontWeight.w700),
                              )),
                        ),
                        SizedBox(
                          height: screenAwareSize(12.0, context),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: 5.0,
                    right: 0.0,
                    left: 300.0,
                    child:  Container(
                      child: IconButton(
                        icon: Icon(Icons.delete),
                        color: Colors.red,
                        onPressed: () {
                          CardPageState().deleteCardImage(imageIndex);
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Home()),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionsList = query.isEmpty
        ? title
        : title.where((p) => p.startsWith(query)).toList();
    return ListView.builder(itemBuilder: (context, index) =>
        ListTile(
          onTap: () {
            imageIndex = index;
            showResults(context);
          },
          leading: Icon(Icons.picture_in_picture),
          title: RichText(
            text: TextSpan(
                text: suggestionsList[index].substring(0, query.length),
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold),
                children: [
                  TextSpan(
                      text: suggestionsList[index].substring(query.length),
                      style: TextStyle(color: Colors.grey))
                ]),
          ),
        ),
      itemCount: suggestionsList.length,
    );
  }
}