import 'dart:async';

import 'package:access_settings_menu/access_settings_menu.dart';
import 'package:android_intent/android_intent.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_search/cards/data.dart';
import 'package:google_search/cards/page.dart';
import 'package:google_search/cards/pageOne.dart';

//import 'package:google_search/cards/utils.dart';
import 'package:google_search/models/user.dart';
import 'package:google_search/resources/firebase_repository.dart';
import 'package:google_search/screens/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:showcaseview/showcase.dart';
import 'package:showcaseview/showcase_widget.dart';
import 'dart:math';
import 'customLogo.dart';
import 'data.dart';
import 'dataSearch.dart';

String currentProfilePic = "https://scontent.fevn2-1.fna.fbcdn.net/v/t1.0-9/70972958_2359766924142450_5817030201632096256_n.jpg?_nc_cat=106&_nc_oc=AQkPKTvTk8UIaqOXQn2e-AnJIgiJPx2BRBsL5w-f2gYrRV2jXkVW5-Y28Hz38E6U8w8&_nc_ht=scontent.fevn2-1.fna&oh=8d5cf8abae6fb80bfde13eb483b9775b&oe=5E289B6F";
String otherProfilePic = "https://blog.hubspot.com/hubfs/image8-2.jpg";
const kGoogleApiKey = "AIzaSyA5eXHq5EHS0qJ5oL4Yp9nl6mOW0XNNE5U";
String otherProfileMail = 'mnacakan007@gmail.com';
String otherProfileName = 'Mnatsakan Manukyan';
var widgetAspectRatio = cardAspectRatio * 1.2;
var cardAspectRatio = 12.0 / 16.0;
int imageIndex = 0;
var myPhotoList;

class CardPage extends StatefulWidget {
  @override
  CardPageState createState() => CardPageState();
}

class CardPageState extends State<CardPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  FirebaseRepository _repository = FirebaseRepository();
  bool isLoginPressed = false;
  User user = User();
  String userNameInitial = '';
  bool resultSettingsOpening = false;

  void switchAccounts() {
    String picBackup = currentProfilePic;
    this.setState(() {
      currentProfilePic = otherProfilePic;
      otherProfilePic = picBackup;
    });
  }

  var currentPage = 100.0;
  PlacesDetailsResponse place;
  String pId;
  PageController pageController;
  String image;
  dynamic imageList;
  String titleText;
  String website;
  String url;
  String phoneNumber;
  int cardIndex;
  String errorMessage;
  String errorLoading;
  var subscription;
  String deleteTitle = 'Egipt';
  String deleteImage = 'https://look.com.ua/pic/201701/1400x1050/look.com.ua-195107.jpg';


  GlobalKey _one = GlobalKey();
  GlobalKey _two = GlobalKey();
  GlobalKey _three = GlobalKey();
  GlobalKey _four = GlobalKey();

  @override
  void initState() {
    super.initState();
    _checkGps();

    addPlace();

    getValuesSF();

    checkConnection();
  }

  @override
  dispose() {
    super.dispose();
    subscription.cancel();
  }


  @override
  Widget build(BuildContext context) {
    SharedPreferences preferences;

    displayShowcase() async {
      preferences = await SharedPreferences.getInstance();
      bool show = preferences.getBool("displayShowcase");
      if (show == null) {
        preferences.setBool('displayShowcase', false);
        return true;
      }

      return false;
    }

    displayShowcase().then((status) {
      if (status) {
        ShowCaseWidget.of(context).startShowCase([
          _one,
          _two,
          _three,
          _four,
        ]);
      }
    });

//    WidgetsBinding.instance.addPostFrameCallback((_) =>
//        ShowCaseWidget.of(context).startShowCase(
//            [_one, _two, _three, _four])
//    );

    PageController controller = PageController(initialPage: 100);
    controller.addListener(() {
      setState(() {
        addPlace();
        currentPage = controller.page;
      });
    });

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  Color(0xFF1b1e44),
                  Color(0xFF2d3447),
                ],
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                tileMode: TileMode.clamp)),
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.transparent,
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      left: 12.0, right: 12.0, top: 30.0, bottom: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Showcase(
                        key: _one,
                        description: 'Tap to see menu options',
                        shapeBorder: CircleBorder(),
                        child: IconButton(
                          icon: Icon(
                            Icons.menu,
                            color: Colors.white,
                            size: 30.0,
                          ),
                          onPressed: () =>
                              _scaffoldKey.currentState.openDrawer(),
                        ),
                      ),
                      Showcase(
                          key: _two,
                          description: 'Tap to see search options',
                          shapeBorder: CircleBorder(),
                          child: IconButton(
                            icon: Icon(
                              Icons.search,
                              color: Colors.white,
                              size: 30.0,
                            ),
                            onPressed: () {
                              showSearch(
                                  context: context, delegate: DataSearch());
                            },
                          )
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Showcase(
                        key: _three,
                        description: 'Tap to see Favorite Places',
                        child: Text("Favorite Places",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 46.0,
                              fontFamily: "Calibre-Semibold",
                              letterSpacing: 1.0,
                            )),
                      ),
//                    IconButton(
//                      icon: Icon(
//                        CustomIcons.option,
//                        size: 12.0,
//                        color: Colors.white,
//                      ),
//                      onPressed: () {},
//
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: Color(0xFFff6e6e),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 22.0, vertical: 6.0),
                            child: Text("Animated",
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 15.0,
                      ),
                      Text("25+ Stories",
                          style: TextStyle(color: Colors.blueAccent))
                    ],
                  ),
                ),
                Stack(
                  children: <Widget>[
                    Showcase(
                      key: _four,
                      description: 'Tap to see Cards when they appear',
                      shapeBorder: CircleBorder(),
                      child: Container(
                        height: 300.0,
                        padding: EdgeInsets.fromLTRB(20.0, 0.0, 70.0, 0),
                        child: Center(
                          child: Text(noCardMessage(),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontFamily: "Calibre-Semibold",
                                letterSpacing: 1.0,
                              )),
                        ),
                      ),
                    ),
                    CardScrollWidget(currentPage),
                    Positioned.fill(
                      child: PageView.builder(
                        itemCount: images.length,
                        controller: controller,
                        reverse: true,
                        itemBuilder: (context, index) {
                          return Container();
                        },
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Last Deleted Card",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 35.0,
                            fontFamily: "Calibre-Semibold",
                            letterSpacing: 1.0,
                          )),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.blueAccent,
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 22.0, vertical: 6.0),
                            child: Text("Latest",
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 15.0,
                      ),
                      Text("9+ Stories",
                          style: TextStyle(color: Colors.blueAccent))
                    ],
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Container(
                          width: 296.0,
                          height: 222.0,
                          child: Padding(
                            padding: EdgeInsets.only(left: 18.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20.0),
                              child: Image.network(deleteImage,
                                  fit: BoxFit.cover),
                            ),
                          ),
                        ),
                        Container(
                          width: 296.0,
                          height: 222.0,
                          alignment: Alignment(-1.0, 1.0),
                          padding: EdgeInsets.fromLTRB(30.0, 20.0, 10.0, 10.0),
                          child: Text(deleteTitle,
                            style: TextStyle(color: Colors.white,
                                fontSize: 20.0,),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
              ],
            ),
          ),
          drawer: Drawer(
            child: ListView(
              children: <Widget>[
                Center(
                  child: UserAccountsDrawerHeader(
                    accountEmail: Text(otherProfileName ?? ''),
                    accountName: Text(otherProfileMail ?? ''),
                    currentAccountPicture: GestureDetector(
                      child: Hero(
                        tag: "hero1",
                        child: ClipOval(
                          child: CustomLogo(
                            size: 60.0,
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                fullscreenDialog: true,
                                builder: (BuildContext context) => Page1()));
                      },
                    ),
                    otherAccountsPictures: <Widget>[
                      GestureDetector(
                        child: CircleAvatar(
                          backgroundColor: Colors.deepPurple,
                          child: Text(userNameInitial,
                              style: TextStyle(color: Colors.white)
                          ),
                        ),
                        onTap: switchAccounts,
                      ),
                    ],
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/images/hq.jpg"),
                            fit: BoxFit.fill
                        )
                    ),
                  ),
                ),
                ListTile(
                    title: Text("Page One"),
                    trailing: Icon(Icons.arrow_upward),
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              Page()));
                    }
                ),
                ListTile(
                    title: Text("Page Two"),
                    trailing: Icon(Icons.arrow_right),
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              Page()));
                    }
                ),
                Divider(),
                ListTile(
                  title: Text("Cancel"),
                  trailing: Icon(Icons.cancel),
                  onTap: () => Navigator.pop(context),
                ),
                Divider(),
                ListTile(
                  title: Text("SignOut"),
                  trailing: Icon(Icons.exit_to_app),
                  onTap: () {
                    performLogOut();
                    _repository.signIn().then((FirebaseUser user) {
                      if (user != null) {
                        _repository.deleteData(user);
                      } else {
                        print("There was an error");
                      }
                    });
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future _checkGps() async {
    if (!(await Geolocator().isLocationServiceEnabled())) {
      if (Theme
          .of(context)
          .platform == TargetPlatform.android) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Can't get gurrent location"),
              content:
              const Text('Please make sure you enable GPS and try again'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () async {
                    final AndroidIntent intent = new AndroidIntent(
                        action: 'android.settings.LOCATION_SOURCE_SETTINGS');
                    await intent.launch();
//                    openSettingsMenu("ACTION_WIRELESS_SETTINGS");
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    }
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(
              title: Text('Do you really want to exit the app?'),
              actions: <Widget>[
                FlatButton(
                  child: Text('No'),
                  onPressed: () => Navigator.pop(context, false),
                ),
                FlatButton(
                  child: Text('Yes'),
                  onPressed: () => Navigator.pop(context, true),
                )
              ],
            )
    );
  }

  openSettingsMenu(settingsName) async {
    try {
      resultSettingsOpening =
      await AccessSettingsMenu.openSettings(settingsType: settingsName);
    } catch (e) {
      resultSettingsOpening = false;
    }
  }

  Future checkConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      print("Connected to Mobile Network");
    } else if (connectivityResult == ConnectivityResult.wifi) {
      print("Connected to WiFi");
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) => locationDialog()
      );
      print("Unable to connect. Please Check Internet Connection");
    }
  }

  checkConnectionOnline() {
    subscription = Connectivity().onConnectivityChanged.listen((
        ConnectivityResult result) {
      print("Connection Status has Changed");
    });
  }

  Widget locationDialog() {
    return CupertinoAlertDialog(
      title: Text("Unable to connect. Please Check Internet Connection"),
      actions: <Widget>[
        CupertinoDialogAction(
            child: const Text('Settings'),
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pop(context, '');
              openSettingsMenu("ACTION_WIRELESS_SETTINGS");
            }),
        CupertinoDialogAction(
            child: const Text('Cancel'),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context, 'Cancel');
            }),
      ],
    );
  }

  performLogOut() {
    print("tring to perform logout");
    _repository.signOut();
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (context) {
          return LoginScreen();
        }));
  }

  noCardMessage() {
    var text;
    if (currentPage == 0.0) {
      text = "You do not have a favorite card";
    } else {
      text = "";
    }
    return text;
  }

  addPlace() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      images.clear();
      title.clear();
      imagesList.clear();
      data.clear();
      deleteImage = prefs.getString('deleteImgKey') ?? deleteImage;
      deleteTitle = prefs.getString('deleteTitleKey') ?? deleteTitle;
      final myStringList = prefs.getStringList('dateImage') ?? [];
      final myStringList2 = prefs.getStringList('dateTitle') ?? [];
      final myStringList3 = prefs.getStringList('dateImages') ?? [];
      for (var i = 0; i < myStringList.length; i++) {
        images.add(myStringList[i]);
      }
      for (var j = 0; j < myStringList2.length; j++) {
        title.add(myStringList2[j]);
      }
      for (var p = 0; p < myStringList3.length; p++) {
        data.add(myStringList3[p]);
      }
    });
  }

  deleteCardImage(index) async {
    final prefs = await SharedPreferences.getInstance();
    final deleteImgKey = 'deleteImgKey';
    final deleteTitleKey = 'deleteTitleKey';
    var myImageData = prefs.getStringList('dateImage') ?? [];
    var myTitleData = prefs.getStringList('dateTitle') ?? [];
    prefs.setString(deleteImgKey, myImageData[index]);
    prefs.setString(deleteTitleKey, myTitleData[index]);
    myImageData.removeAt(index);
    myTitleData.removeAt(index);
    prefs.setStringList('dateImage', myImageData);
    prefs.setStringList('dateTitle', myTitleData);
  }

  getValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String firePhoto = prefs.getString('firePhoto') ?? '';
    String fireEmail = prefs.getString('fireEmail') ?? '';
    String fireName = prefs.getString('fireName') ?? '';

    setState(() {
      if (fireName != '') {
        userNameInitial = fireName.split(" ").map((n) => n[0])?.join(".");
      }
      if (firePhoto != '') {
        var firePhotoBig = firePhoto.split('=') ?? [];
        currentProfilePic = '${firePhotoBig[0]}' '=s960-c' ?? '';
      }
      otherProfileMail = fireName;
      otherProfileName = fireEmail;
    });
  }

}

// ignore: must_be_immutable
class CardScrollWidget extends StatelessWidget {
  var currentPage;
  var padding = 20.0;
  var verticalInset = 20.0;

  CardScrollWidget(this.currentPage);

  @override
  Widget build(BuildContext context) {
    return new AspectRatio(
      aspectRatio: widgetAspectRatio,
      child: LayoutBuilder(builder: (context, contracts) {
        var width = contracts.maxWidth;
        var height = contracts.maxHeight;

        var safeWidth = width - 2 * padding;
        var safeHeight = height - 2 * padding;

        var heightOfPrimaryCard = safeHeight;
        var widthOfPrimaryCard = heightOfPrimaryCard * cardAspectRatio;

        var primaryCardLeft = safeWidth - widthOfPrimaryCard;
        var horizontalInset = primaryCardLeft / 2;

        List<Widget> cardList = new List();

        for (var i = 0; i < images.length; i++) {
          var delta = i - currentPage;
          bool isOnRight = delta > 0;

          var start = padding +
              max(
                  primaryCardLeft -
                      horizontalInset * -delta * (isOnRight ? 15 : 1),
                  0.0);

          var cardItem = Positioned.directional(
            top: padding + verticalInset * max(-delta, 0.0),
            bottom: padding + verticalInset * max(-delta, 0.0),
            start: start,
            textDirection: TextDirection.rtl,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16.0),
              child: Container(
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Colors.black12,
                      offset: Offset(3.0, 6.0),
                      blurRadius: 10.0)
                ]),
                child: AspectRatio(
                  aspectRatio: cardAspectRatio,
                  child: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      Image.network(images[i], fit: BoxFit.cover),
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.0, vertical: 8.0),
                              child: Text(title[i],
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 25.0,
                                      fontFamily: "SF-Pro-Text-Regular")),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 12.0, bottom: 12.0),
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 22.0, vertical: 6.0),
                                decoration: BoxDecoration(
                                    color: Colors.blueAccent,
                                    borderRadius: BorderRadius.circular(20.0)),
                                child: Text("Read Later",
                                    style: TextStyle(color: Colors.white)),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),

          );

          cardList.add(cardItem);
        }

        return Stack(
          children: cardList,
        );
      }),
    );
  }
}



