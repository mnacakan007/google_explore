import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_search/cards/customLogo.dart';

import 'cardPage.dart';

class Page1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/hq.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: Stack(
          children: <Widget>[
            Center(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(30.0),
                  ),
                  Text('User', style: TextStyle(color: Colors.red, fontSize: 25.0)),
                  Divider(),
                  Text(otherProfileMail ?? '', style: TextStyle(color: Colors.white, fontSize: 20.0)),
                  Divider(),
                  Text(otherProfileName ?? '', style: TextStyle(color: Colors.white, fontSize: 20.0)),
                  Divider(),
                ],
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Hero(
                tag: "hero1",
                child: Container(
                  height: 300.0,
                  width: 300.0,
                  child: CustomLogo(),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0.0, 30.0, 10.0, 10.0),
              child: FlatButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Icon(Icons.arrow_back, color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}