import 'dart:async';
//import 'dart:math' as math;
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:google_maps_webservice/places.dart';


//class Page extends StatelessWidget {
//
//  final String title;
//
//  Page(this.title);
//
//  @override
//  Widget build(BuildContext context) {
//    return new Scaffold(
//      appBar: new AppBar(title: new Text(title), backgroundColor: Colors.redAccent,),
//      body: new Center(
//        child: new Text(title),
//      ),
//    );
//  }
//}

class Page extends StatefulWidget {
  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<Page> {
  String animationName = 'idle';
  GoogleMapController mapController;
  PlacesDetailsResponse place;
  Widget expandedChild;
  bool isPaused = false;
  bool isLoading = false;


  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      Color(0xFF1b1e44),
                      Color(0xFF2d3447),
                    ],
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    tileMode: TileMode.clamp)),
            child: Scaffold(
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                title: Text("Animation"),
                centerTitle: true,
                backgroundColor: Color(0xFF1b1e44),
                actions: <Widget>[
                  IconButton(
                    onPressed: () {
                      setState(() {
                        isPaused = false;
                        animationName = 'move_phone';
                        Timer(Duration(milliseconds: 100), reset);
                      });
                    },
                    icon: Icon(
                      Icons.replay,
                      size: 20,
                    ),
                  ),
                ],
              ),
              body: GestureDetector(
                  onTap: () {
                    setState(() {
                      animationName = 'move_phone';
                      Timer(Duration(milliseconds: 2000), _flare2);
                    });
                  },
                  child: _flare()
              ),
            ),
          ),// rotate -10 deg
          isPaused ? Container(
            width: 150.0,
            height: 312.0,
            margin: EdgeInsets.fromLTRB(98.0, 237.0, 0.0, 0.0),
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
                bottomRight: Radius.circular(20),
                bottomLeft: Radius.circular(20),
              ),
              child: Container(
                alignment: Alignment.center,
                child: _body(),
              ),
            ),
          ) : Container(),
//          isPaused ? RotationTransition(
//            turns: new AlwaysStoppedAnimation(14.5 / 360),
//            child: Container(
//              width: 150.0,
//              height: 310.0,
//              decoration: new BoxDecoration(
//                  color: Colors.green,
//                  borderRadius: new BorderRadius.only(
//                      topLeft: const Radius.circular(40.0),
//                      topRight: const Radius.circular(40.0))),
//              margin: EdgeInsets.fromLTRB(68.0, 268.0, 0.0, 0.0),
//              //padding: EdgeInsets.all(10),
//              child: _body(),
//            ),
//          ) : Container(),
        ],
      ),
    );
  }


  buildRotatedContainer() {
    return Transform.rotate(
        angle: 3.14 / 4,
        origin: Offset(0, 10),
        child: Container(
          color: Colors.blue,
          width: 100,
          height: 100,
        ));
  }

  Widget _flare() {
    return FlareActor(
      'assets/Filip(3).flr',
      alignment: Alignment.center,
      fit: BoxFit.contain,
      animation: animationName,
      isPaused: isPaused,
    );
  }

  _flare2() {
    setState(() {
      animationName = 'phone_sway';
      isLoading = false;
      isPaused = true;
    });
  }

  reset() {
    setState(() {
      animationName = 'idle';
    });
  }

  Widget _body() {
//    if (isLoading) {
//      return Center(child: CircularProgressIndicator(value: null));
//    } else {
    return GoogleMap(
      onMapCreated: _onMapCreated,
      options: GoogleMapOptions(
        scrollGesturesEnabled: true,
        tiltGesturesEnabled: true,
        rotateGesturesEnabled: true,
        myLocationEnabled: true,
        compassEnabled: true,
        mapType: MapType.normal,
        trackCameraPosition: true,
        zoomGesturesEnabled: true,
      ),
    );
  }


  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    if (place != null) {
      final placeDetail = place.result;
      final location = place.result.geometry.location;
      final lat = location.lat;
      final lng = location.lng;
      final center = LatLng(lat, lng);
      var markerOptions = MarkerOptions(
          position: center,
          infoWindowText: InfoWindowText(
              "${placeDetail.name}", "${placeDetail.formattedAddress}"));
      mapController.addMarker(markerOptions);
      mapController.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(target: center, zoom: 16.0)));
    }
    // refresh();
  }

}



