import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'cardPage.dart';

class CustomLogo extends StatefulWidget {
  final double size;

  CustomLogo({this.size = 200.0});

  @override
  _CustomLogoState createState() => _CustomLogoState();
}

class _CustomLogoState extends State<CustomLogo> {

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: widget.size,
      height: widget.size,
      child: Center(
        child:Image.network(
          currentProfilePic == '=s960-c' ? otherProfilePic : currentProfilePic,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}