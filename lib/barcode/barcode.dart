import 'dart:async';
import 'dart:io';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:url_launcher/url_launcher.dart';

class BarcodePage extends StatefulWidget {
  @override
  _BarcodePageState createState() => _BarcodePageState();
}


class _BarcodePageState extends State<BarcodePage> {
  String barcode = "";

  @override
  initState() {
    super.initState();
    barcodeScanning();
  }

  File galleryFile;

  imageSelectorGallery() async {
    galleryFile = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      // maxHeight: 50.0,
      // maxWidth: 50.0,
    );
    print("You selected gallery image : " + galleryFile.path);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
            appBar: new AppBar(
              backgroundColor: Colors.green,
              title: new Text('Scan Barcode'),
            ),
            body: new Center(
              child: new Column(
                children: <Widget>[
                  new Container(
                    child: new RaisedButton(
                        onPressed: barcodeScanning, child: new Text("Capture image")),
                    padding: const EdgeInsets.all(8.0),
                  ),
                  new Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      child: Text(
                        "Barcode Number after Scan : $barcode"),
                      onTap: () async {
                        if (await canLaunch(barcode)) {
                          await launch(barcode);
                        } else {
                          throw 'Barcode Number after Scan : $barcode';
                        }
                      },
                    ),
                  ),
                  displayImage(),
                ],
              ),
            ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (context)=>AlertDialog(
          title: Text('Do you really want to exit the app?'),
          actions: <Widget>[
            FlatButton(
              child: Text('No'),
              onPressed: ()=> Navigator.pop(context, false),
            ),
            FlatButton(
              child: Text('Yes'),
              onPressed: ()=> Navigator.pop(context, true),
            )
          ],
        )
    );
  }

  Widget displayImage() {
    return new SizedBox(
      height: 300.0,
      width: 400.0,
      child: galleryFile == null
          ? new Text('Sorry nothing to display', textAlign: TextAlign.center,)
          : new Image.file(galleryFile),
    );
  }

// Method for scanning barcode....
  Future barcodeScanning() async {
  imageSelectorGallery();

    try {
      String barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'No camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException {
      setState(() => this.barcode =
      'Nothing captured.');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }
  }
}
