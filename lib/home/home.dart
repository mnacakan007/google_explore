import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_search/barcode/barcode.dart';
import 'package:google_search/map/map.dart';
import '../cards/cardPage.dart';
import '../map/map.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class Home extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends State<Home> {
//  int selectedPage = 0;
//  final _pageOptions = [CardPage(),BarcodePage(),MapsDemo()];
//  static const TextStyle optionStyle = TextStyle(
//      fontSize: 30, fontWeight: FontWeight.bold);
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//    body: _pageOptions[selectedPage],
//      bottomNavigationBar: BottomNavigationBar(
//        items: const <BottomNavigationBarItem>[
//          BottomNavigationBarItem(
//            icon: Icon(Icons.home),
//            title: Text('Home'),
//          ),
//          BottomNavigationBarItem(
//            icon: Icon(Icons.crop_free),
//            title: Text('Scanner'),
//          ),
//          BottomNavigationBarItem(
//            icon: Icon(Icons.map),
//            title: Text('Map'),
//          ),
//        ],
//        currentIndex: selectedPage,
//        selectedItemColor: Colors.amber[800],
//        onTap: (int index) {
//            setState(() {
//              selectedPage = index;
//            });
//        },
//      ),
//    );
//  }


  int _page = 0;
  final _pageOptions = [CardPage(), BarcodePage(), Maps()];
  static const TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  GlobalKey _bottomNavigationKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: CurvedNavigationBar(
          key: _bottomNavigationKey,
          index: 0,
          height: 50.0,
          items: <Widget>[
            Icon(Icons.home, size: 30),
            Icon(Icons.crop_free, size: 30),
            Icon(Icons.map, size: 30),
          ],
          color: Colors.white,
          buttonBackgroundColor: Colors.white,
          backgroundColor: Colors.green[200],
          animationCurve: Curves.easeInOut,
          animationDuration: Duration(milliseconds: 600),
          onTap: (index) {
            setState(() {
              _page = index;
            });
          },
        ),
        body: _pageOptions[_page],
    );
  }

}