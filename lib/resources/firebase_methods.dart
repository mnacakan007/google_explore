import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/user.dart';
import '../utils/utilities.dart';

class FirebaseMethods {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  GoogleSignIn _googleSignIn = GoogleSignIn();
  static final Firestore firestore = Firestore.instance;
  dynamic id;
  //user class
  User user = User();


  Future<FirebaseUser> getCurrentUser() async {
    FirebaseUser currentUser;
    currentUser = await _auth.currentUser();
    return currentUser;
  }

  Future<FirebaseUser> signIn() async {
    GoogleSignInAccount _signInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication _signInAuthentication =
    await _signInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: _signInAuthentication.accessToken,
        idToken: _signInAuthentication.idToken);

    FirebaseUser user = await _auth.signInWithCredential(credential);
    return user;
  }

  Future<bool> authenticateUser(FirebaseUser user) async {
    QuerySnapshot result = await firestore
        .collection("users")
        .where("email", isEqualTo: user.email)
        .getDocuments();

    final List<DocumentSnapshot> docs = result.documents;

    //if user is registered then length of list > 0 or else less than 0
    return docs.length == 0 ? true : false;
  }

  Future<void> addDataToDb(FirebaseUser currentUser) async {
    String username = Utils.getUsername(currentUser.email);

    user = User(
        uid: currentUser.uid,
        email: currentUser.email,
        name: currentUser.displayName,
        profilePhoto: currentUser.photoUrl,
        username: username);

    final prefs = await SharedPreferences.getInstance();
    final firePhoto = "firePhoto";
    final fireUserName = 'fireUserName';
    final fireEmail = 'fireEmail';
    final fireName = 'fireName';

    prefs.setString(firePhoto, user.profilePhoto);
    prefs.setString(fireUserName, user.username);
    prefs.setString(fireEmail, user.email);
    prefs.setString(fireName, user.name);

    firestore
        .collection("users")
        .document(currentUser.uid)
        .setData(user.toMap(user));
  }


  Future<void> signOut() async {
    final prefs = await SharedPreferences.getInstance();
    await _googleSignIn.disconnect();
    await _googleSignIn.signOut();
    prefs.remove("firePhoto");
    prefs.remove("fireUserName");
    prefs.remove("fireEmail");
    prefs.remove("fireName");
    return await _auth.signOut();
  }


  deleteData(FirebaseUser currentUser) {
    firestore
        .collection('users')
        .document(currentUser.uid).delete()
        .catchError((onError) {
          print(onError);
        });
  }

}